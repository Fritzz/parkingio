// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

//= require underscore
//= require gmaps/google
$(document).ready(function(){
  if ($("#results").length > 0 && $("#results").data('locations')[0] != undefined){
    $("#results").data('initialized',false)
    var handler;
    drawMap();
  };
});

function drawMap(){
  if ($("#results").data('initialized') == true){
    setTimeout(function(){removeMarkers();getMarkers();drawMarkers();}, 400);
  } else {
    getMarkers();
    handler = Gmaps.build('Google');
    handler.buildMap({ provider: {maxZoom: 15}, internal: {id: 'map'}}, function(){
      drawMarkers();
    });
    $("#results").data('initialized', true)
  };
};

function getMarkers(){
  window.markers = $("#results").data("locations")
  window.circles = $("#results").data("circles")
}

function removeMarkers(){
  handler.removeMarkers(window.markers)
}

function drawMarkers(){
  markers = handler.addMarkers(window.markers);
  handler.resetBounds();
  handler.bounds.extendWith(markers);
  handler.fitMapToBounds();
  handler.addCircles(window.circles);
}




$(document).ready(function() {
  var completer;
  completer = new GmapsCompleter(
    {
      inputField: '#gmaps-input-address',
      errorField: '#gmaps-error'
    }
  );
  completer.autoCompleteInit({region: 'DE'});
});
