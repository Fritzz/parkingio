# == Schema Information
#
# Table name: trips
#
#  id          :bigint(8)        not null, primary key
#  name        :string
#  origin      :decimal(8, 6)    default([]), is an Array
#  pick_up     :decimal(8, 6)    default([]), is an Array
#  drop_off    :decimal(8, 6)    default([]), is an Array
#  destination :decimal(8, 6)    default([]), is an Array
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Trip < ApplicationRecord


  def self.berlin
    [{"lat": 52.520008 ,"lng": 13.404954}].to_json

  end

  def marker_for(latlong)
    [{"lat": latlong[0] ,"lng": latlong[1]}].to_json

  end


  def origin_marker
    marker_for(origin)
  end


  # def markers
  #   {"lat": lat ,"lng": long}
  #
  # end

end
