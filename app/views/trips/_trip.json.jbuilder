json.extract! trip, :id, :name, :origin, :pick_up, :drop_off, :destination, :created_at, :updated_at
json.url trip_url(trip, format: :json)
