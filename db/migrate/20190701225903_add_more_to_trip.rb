class AddMoreToTrip < ActiveRecord::Migration[5.2]
  def change
    add_column :trips, :origin_address, :string
    add_column :trips, :destination_address, :string
    add_column :trips, :origin_service, :integer
    add_column :trips, :destination_service, :integer
  end
end
