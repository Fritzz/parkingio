class CreateTrips < ActiveRecord::Migration[5.2]
  def change
    create_table :trips do |t|
      t.string :name
      t.decimal :origin, array: true, precision: 8, scale: 6, default: []
      t.decimal :pick_up, array: true, precision: 8, scale: 6, default: []
      t.decimal :drop_off, array: true, precision: 8, scale: 6, default: []
      t.decimal :destination, array: true, precision: 8, scale: 6, default: []
      t.timestamps
    end
  end
end
